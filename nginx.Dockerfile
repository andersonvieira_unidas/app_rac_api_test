FROM nginx:1.13

COPY nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/local/apache2/htdocs/
