const request = require('supertest');
const moment = require("moment");

// import plain Joi 
var Joi = require('joi');
//import the module 
var joiAssert = require('joi-assert');

//Import Project Modules
const { schemaTarifaPadrao, schemaCotacao } = require('../schemas/Cotacao');

const { schemaFormater, loginRequest } = require('./module');

const { api_unidas } = require('./end_point')
const { user_production } = require('./usuarios');

//import mochawesome optionals
const addContext = require('mochawesome/addContext');
var rateQualifier = 0

describe('Suíte Cotação', function () {

    it('Buscar Tarifa Padrão', function (done) {
        this.timeout(10000);
        loginRequest(user_production.cpf, user_production.password, function (userInformation) {
            request(api_unidas.https_base_path)
                .get(api_unidas.api_cotacao_tarifa_padrao)
                .set('Authorization', userInformation.token)
                .set('Accept', 'application/json')
                .type('json')
                .end(function (err, res) {
                    this.timeout(10000);
                    rateQualifier = res.body.Objeto[0].CfgValor

                    Joi.assert(res.body, schemaTarifaPadrao, { abortEarly: false }, (err, data) => {
                        if (err) throw err;
                    });
                    done(err);
                })
        })
        addContext(this, schemaFormater(schemaTarifaPadrao));
    });

    it('Buscar Cotação', function (done) {
        var url = api_unidas.api_cotacao
        this.timeout(50000);

        const pickupDate = moment().add(15, 'days').format("YYYY-MM-DD HH:00:00")
        const returnDate = moment().add(16, 'days').format("YYYY-MM-DD HH:00:00")

        url = url.replace('{0}', pickupDate); 
        url = url.replace('{1}', returnDate); 
        url = url.replace('{2}', rateQualifier); 

        loginRequest(user_production.cpf, user_production.password, function (userInformation) {
            request(api_unidas.https_base_path)
                .get(url)
                .set('Authorization', userInformation.token)
                .set('Accept', 'application/json')
                .type('json')
                .end(function (err, res) {
                    Joi.assert(res.body, schemaCotacao, { abortEarly: false }, (err, data) => {
                        if (err) throw err;
                    });
                    done(err);
                })
        })
        addContext(this, schemaFormater(schemaCotacao));
    });
})
