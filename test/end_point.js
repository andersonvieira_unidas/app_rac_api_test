const api_unidas = {
	https_base_path:'https://gtw-apprac.unidas.com.br',

	//lojas
	api_lojas:'/LojasAll/0/S',

	//login
	api_login: '/LoginContaCliente',

	//reservas
	api_listar_reservas: '/V2/ListarMinhasReservas/',

	//home
	api_home_ultima_reserva: '/V2/UltimaReserva/',

	//cotação
	api_cotacao_tarifa_padrao: '/v1/listarconfiguracaosyscfg?chave=TARIFA_PADRAO_APP',
	api_cotacao: '/RealizarCotacaoMobile?companyName=380&pickUpDateTime={0}&pickUpLocation=GRU1&returnDateTime={1}&rateQualifier={2}&returnLocation=GRU1',

	//pagamento
	api_consulta_pagamento_parcelado: '/pagamento/PagamentoParcelado?Valor=120.9',
}

module.exports = { api_unidas }