const request = require('supertest');

// import plain Joi 
var Joi = require('joi');
//import the module 
var joiAssert = require('joi-assert');

//Import Project Modules
const { schemaUltimaReserva } = require('../schemas/Home');

const { schemaFormater, loginRequest } = require('./module');

const { api_unidas } = require('./end_point')
const { user_production } = require('./usuarios');

//import mochawesome optionals
const addContext = require('mochawesome/addContext');

describe('Suite de Home', function () {
    this.timeout(100000);
    it('Buscar Última Reserva', function (done) {
        this.timeout(10000);
        loginRequest(user_production.cpf, user_production.password, function (userInformation) {
            request(api_unidas.https_base_path)
                .get(api_unidas.api_home_ultima_reserva + user_production.cpf)
                .set('Authorization', userInformation.token)
                .set('Accept', 'application/json')
                .type('json')
                .end(function (err, res) {
                    this.timeout(10000);
                    Joi.assert(res.body, schemaUltimaReserva, { abortEarly: false }, (err, data) => {
                        if (err) throw err;
                    });
                    done(err);
                })
        })
        addContext(this, schemaFormater(schemaUltimaReserva));
    });
})
