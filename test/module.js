const request = require('supertest');
//import plain Joi 
var Joi = require('joi');
const { api_unidas, api_unidas_hml } = require('./end_point.js')
const { user_homolog, user_production } = require('./usuarios')
//import mochawesome optionals
const addContext = require('mochawesome/addContext');
convert = require('joi-to-json-schema')
const { schemaLogin } = require('../schemas/Login');
var assert = require('assert');

schemaFormater = (currentSchema) => {

    var esquema = JSON.stringify(convert(currentSchema));

    esquema = esquema.split("additionalProperties");
    esquema = esquema[0];
    esquema = esquema.replace(/\[/g, '');
    esquema = esquema.replace(/\]/g, '');
    esquema = esquema.replace(/{"type":"object","properties":{/g, '');
    esquema = esquema.replace(/"additionalProperties":true,"patterns":,"required":/g, '');
    esquema = esquema.replace(/"array","boolean","number","object","string","null"/g, ' Any.Allow');
    esquema = esquema.replace(/"additionalProperties":true,"patterns":,/g, '');
    esquema = esquema.replace(/,/g, '\n');
    esquema = esquema.replace(/"type":"/g, 'type = ');
    esquema = esquema.replace(/"/g, '');
    esquema = esquema.replace(/:/g, '');
    esquema = esquema.replace(/}/g, '');
    esquema = esquema.replace(/{/g, ' ');

    return esquema;
}
var loginRequest = function (numeroCPF, numeroPIN, callback) {
    request(api_unidas.https_base_path)
        .post(api_unidas.api_login)
        .send({ nrPin: numeroPIN })
        .send({ mobileGUID: '123' })
        .send({ nrDoc: numeroCPF})
        .set('Accept', 'application/json')
        .type('json')
        .end(function (err, res) {
            this.timeout(10000);
            var userInformation = {
                token: res.body.ContaClienteToken/*,
                ano: res.body.EfetuarLoginResult.DataNascimentoAno,
                dia: res.body.EfetuarLoginResult.DataNascimentoDia,
                mes: res.body.EfetuarLoginResult.DataNascimentoMes,
                idCliente: res.body.EfetuarLoginResult.IdCliente,
                cpf: res.body.EfetuarLoginResult.Cpf,*/
            };
            //console.log(res.header['x-server']);
            if (callback) callback(userInformation);
        })
}

module.exports = { schemaFormater, loginRequest }