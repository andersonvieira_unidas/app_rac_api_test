const request = require('supertest');

// import plain Joi 
var Joi = require('joi');
//import the module 
var joiAssert = require('joi-assert');

//Import Project Modules
const { schemaLoja } = require('../schemas/Lojas');
const { schemaFormater } = require('./module');
const { api_unidas } = require('./end_point')

//import mochawesome optionals
const addContext = require('mochawesome/addContext');

describe('Suite de Lojas', function () {
    this.timeout(100000);
    it('Buscar Lojas', function (done) {
        request(api_unidas.https_base_path)
            .get(api_unidas.api_lojas)
            .type('json')
            .end(function (err, res) {
                this.timeout(100000);
                Joi.assert(res.body, schemaLoja, { abortEarly: false }, (err, data) => {
                    if (err) throw err;
                });
                done(err);
            })
        addContext(this, schemaFormater(schemaLoja));
    });

    it('Buscar Lojas com filtro', function (done) {
        request(api_unidas.https_base_path)
            .get(api_unidas.api_lojas + "?filtro=GRU")
            .type('json')
            .end(function (err, res) {
                this.timeout(100000);
                Joi.assert(res.body, schemaLoja, { abortEarly: false }, (err, data) => {
                    if (err) throw err;
                });
                done(err);
            })
        addContext(this, schemaFormater(schemaLoja));
    });
})
