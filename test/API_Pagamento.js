const request = require('supertest');

// import plain Joi 
var Joi = require('joi');
//import the module 
var joiAssert = require('joi-assert');

//Import Project Modules
const { schemaConsultaParcela } = require('../schemas/Pagamento');

const { schemaFormater, loginRequest } = require('./module');

const { api_unidas } = require('./end_point')
const { user_production } = require('./usuarios');

//import mochawesome optionals
const addContext = require('mochawesome/addContext');

describe('Suíte Pagamentos', function () {

    it('Consulta a quantidade de parcelas ao receber um valor', function (done) {
        this.timeout(10000);
        loginRequest(user_production.cpf, user_production.password, function (userInformation) {
            request(api_unidas.https_base_path)
                .post(api_unidas.api_consulta_pagamento_parcelado)
                .set('Authorization', userInformation.token)
                .set('Accept', 'application/json')
                .type('json')
                .end(function (err, res) {
                    this.timeout(10000);
                    Joi.assert(res.body, schemaConsultaParcela, { abortEarly: false }, (err, data) => {
                        if (err) throw err;
                    });
                    done(err);
                })
        })
        addContext(this, schemaFormater(schemaConsultaParcela));
    });
})