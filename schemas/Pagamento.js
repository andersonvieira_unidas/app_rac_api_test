var Joi = require('joi');

const schemaConsultaParcela = Joi.array().allow(null).items(Joi.object({
    PARCELAS: Joi.number().required()
}).unknown().required());

module.exports = { schemaConsultaParcela };
