var Joi = require('joi');

const schemaLogin = Joi.object({
	access_token: Joi.string().required(),
	token_type: Joi.string().required(),
	refresh_token: Joi.string().required()
}).unknown().required()

module.exports = { schemaLogin };