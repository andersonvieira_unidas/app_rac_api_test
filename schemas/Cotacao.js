var Joi = require('joi');

const schemaTarifaPadrao = Joi.object({
    Objeto: Joi.array().items(Joi.object({
        CfgValor: Joi.string().required(),
    }).unknown())
}).unknown().required()


const schemaCotacao = Joi.object({
    HttpStatusCode: Joi.number().required(),
    Objeto: Joi.object({
        Quotations: Joi.array().items(Joi.object({
            VehAvail: Joi.object({
                Status: Joi.string().required(),
                Vehicle: Joi.object({
                    PassengerQuantity: Joi.string().required(),
                    Code: Joi.string().required(),
                    VehicleCategory: Joi.string().required(),
                    Name: Joi.string().required(),
                    VehicleGroups: Joi.string().required(),
                    PictureURL: Joi.string().required()
                }).required().unknown(),
                RateDistance: Joi.object({
                    Unlimited: Joi.boolean().required()
                }).required().unknown(),
                VehicleCharge: Joi.object({
                    UnitCharge: Joi.number().required(),
                    UnitChargePer: Joi.number().required(),
                    Quantity: Joi.number().required(),
                    UnitPrePayment: Joi.number().required(),
                }).required().unknown(),
                RateQualifier: Joi.object({
                    RateQualifierNumber: Joi.string().required()
                }).required().unknown(),
                TotalCharge: Joi.object({
                    CurrencyCode: Joi.string().required(),
                    RateTotalAmount: Joi.number().required(),
                    EstimatedTotalAmount: Joi.number().required()
                }).required().unknown(),
                PricedEquip: Joi.array().items(Joi.object({
                    EquipType: Joi.string().required(),
                    Quantity: Joi.number().required(),
                    Description: Joi.string().required(),
                    QuantityItem: Joi.string().allow(null),
                    Charge: Joi.object({
                        Amount: Joi.number().required(),
                        UnitCharge: Joi.number().required(),
                        UnitChargePer: Joi.number().required(),
                        Quantity: Joi.number().required()
                    })
                }).unknown().required())
            }).unknown(),
        }).unknown())
    }).unknown().required()
}).unknown().required()

module.exports = { schemaTarifaPadrao, schemaCotacao };